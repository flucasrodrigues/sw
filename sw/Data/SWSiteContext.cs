﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SW.Site.Models;

namespace SW.Site.Models
{
    public class SWSiteContext : DbContext
    {
        public SWSiteContext (DbContextOptions<SWSiteContext> options)
            : base(options)
        {
        }

        public DbSet<SW.Site.Models.Produto> ProdutoModel { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Produto>().Property(a => a.Id)
                                          .HasColumnName("ProdutoId");

            modelBuilder.Entity<Produto>().HasKey(a => a.Id);

            modelBuilder.Entity<Produto>().Property(a => a.Nome)
                                          .HasColumnType("varchar(100)")
                                          .HasMaxLength(100);
            base.OnModelCreating(modelBuilder);
        }
    }
}
