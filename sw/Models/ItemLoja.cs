﻿using SW.Site.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SW.Site.Models
{
    public class ItemLoja:BaseModel
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public TipoPromocaoEnum? Promocao { get; set; }
        public int Quantidade{ get; set; }
    }
}
