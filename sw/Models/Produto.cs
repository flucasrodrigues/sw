﻿using SW.Site.Enumerators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SW.Site.Models
{
    public class Produto : BaseModel
    {

        [Required(ErrorMessage = "O campo Nome é obrigatório.")]
        [MinLength(2, ErrorMessage = "Digite no mínimo 2 caracteres.")]
        [MaxLength(100, ErrorMessage = "Digite no ,áximo 100 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo Preço é obrigatório.")]
        [Range(1,9999999)]
        public decimal Preco { get; set; }

        public TipoPromocaoEnum? Promocao { get; set; }
    }
}
