using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using SW.Site.Models;
using Microsoft.EntityFrameworkCore;

namespace SW.Site.Controllers
{
    public class ItensLojaController : Controller
    {
        public ItensLojaController(SWSiteContext context)
        {
            _context = context;
        }

        private readonly SWSiteContext _context;
        public async Task<IActionResult> Index()
        {
            var produtos = await _context.ProdutoModel.ToListAsync();
            ItemLoja itemLoja;
            List<ItemLoja> itensLoja = new List<ItemLoja>();
            foreach (var item in produtos)
            {
                itemLoja = new ItemLoja();

                itemLoja.Id = item.Id;
                itemLoja.Nome = item.Nome;
                itemLoja.Preco = item.Preco;
                itemLoja.Promocao = item.Promocao;
                itensLoja.Add(itemLoja);
            }

            return View(itensLoja);
        }

        public async Task<IActionResult> AddToCart(Guid? id, int quantidade)
        {
            
            if (id == null)
            {
                return NotFound();
            }
            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddHours(2);
            var produto = await _context.ProdutoModel
                .SingleOrDefaultAsync(m => m.Id == id);

            KeyValuePair<string, string> produtoCart =
                new KeyValuePair<string, string>("produto_" + produto.Nome, id.Value.ToString() + ";" + quantidade.ToString());

            Response.Cookies.Append("produto_" + produto.Nome, id.Value.ToString() + ";" + quantidade.ToString());
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CheckOut()
        {
            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddHours(2);

            ItemLoja itemLoja;
            List<ItemLoja> listaItemLoja = new List<ItemLoja>();

            foreach (var produtoCarrinho in Request.Cookies.Where(a => a.Key.Contains("produto_")))
            {
                itemLoja = new ItemLoja();
                var id = produtoCarrinho.Value.Split(';')[0];
                var qtd = produtoCarrinho.Value.Split(';')[1];
                var produto = await _context.ProdutoModel
                    .SingleOrDefaultAsync(m => m.Id.ToString() == id);

                itemLoja.Id = produto.Id;
                itemLoja.Nome = produto.Nome;
                itemLoja.Preco = produto.Preco;
                itemLoja.Promocao = produto.Promocao;
                itemLoja.Quantidade = Convert.ToInt32(qtd);
                listaItemLoja.Add(itemLoja);
            }
            decimal totalPagar = 0;
            foreach (var produto in listaItemLoja.Where(a => a.Promocao == Enumerators.TipoPromocaoEnum.PagueUmLeveDois))
            {
                if ((produto.Quantidade % 2) == 0)
                {
                    totalPagar += ((produto.Preco * produto.Quantidade) / 2);
                }
                else
                {
                    totalPagar += (((produto.Preco * (produto.Quantidade - 1)) / 2) + produto.Preco);
                }
            }
            int count = 0;
            foreach (var produto in listaItemLoja.Where(a => a.Promocao == Enumerators.TipoPromocaoEnum.TresPorDez))
            {
                for(int i = 0; i < produto.Quantidade; i++)
                {
                    count++;
                    if(count==3)
                    {
                        totalPagar += 10;
                        count = 0;
                    }
                }
                totalPagar += produto.Preco * count;
            }


            return RedirectToRoute("Index");
        }
    }
}