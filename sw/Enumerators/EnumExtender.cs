﻿//using Microsoft.AspNetCore.Mvc.Rendering;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Reflection;

//namespace SW.Site.Enumerators
//{
//    public static class EnumExtender
//    {
//        public static string ExtGetDescription(this Enum value)
//        {
//            var type = value.GetType();
//            var name = Enum.GetName(type, value);
//            if (name == null) return null;
//            var field = type.GetField(name);
//            if (field == null) return null;
//            var attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
//            return attr?.Description;
//        }

//        public static string ExtGetName(this Enum value)
//        {
//            var type = value.GetType();
//            return Enum.GetName(type, value);
//        }

//        public static int ExtGetValue(this Enum value)
//        {
//            return Convert.ToInt32(value);
//        }

//        public static SelectList ExtToSelectList(this Type enumObj)
//        {
//            var itens = new List<SelectListItem>();
//            var values = Enum.GetValues(enumObj);
//            foreach (Enum value in values)
//            {
//                itens.Add(new SelectListItem
//                {
//                    Value = value.ExtGetValue().ToString(),
//                    Text = value.ExtGetDescription() ?? value.ExtGetName(),
//                });
//            }
//            return new SelectList(itens.AsEnumerable(), "Value", "Text");
//        }
//    }
//}
