﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SW.Site.Enumerators
{
    public enum TipoPromocaoEnum
    {
        [DisplayName("Pague 1 e leve 2")]
        PagueUmLeveDois = 1,
        [DisplayName("Leve 3 produtos por 10 reais")]
        TresPorDez = 2
    }
}
